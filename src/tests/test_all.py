import os
import requests

SERVER_HOST = os.environ.get('HSE_HTTP_TESTS_SERVER_HOST', 'server')
SERVER_PORT = int(os.environ.get('HSE_HTTP_FLASK_PORT', 80))
URL = 'http://' + SERVER_HOST
if SERVER_PORT != 80:
    URL += ':{}'.format(SERVER_PORT)

def test_all():

    animal1 = {'name': 'horse', 'food': ['oats', 'apple'], 'description': "It's name is Cinamon"}
    animal2 = {'name': 'lion', 'food': ['meet']}

    print()

    endpoint = URL + '/zoo/'

    response = requests.post(endpoint, json=animal1)
    id1 = response.json()['animal_id']
    assert response.status_code == 200

    print()
    print('POST', endpoint, animal1)
    print('Status_code:', response.status_code)
    print('Response content:', response.json())

    response = requests.post(endpoint, json={'name': 'lion', 'food': ['meet']})
    id2 = response.json()['animal_id']
    assert response.status_code == 200

    print()
    print('POST', endpoint, animal1)
    print('Status_code:', response.status_code)
    print('Response content:', response.json())

    endpoint = URL + f'/zoo/{id1}'

    response = requests.get(endpoint)
    assert response.status_code == 200
    assert response.json() == animal1

    print()
    print('GET', endpoint)
    print('Status_code:', response.status_code)
    print('Response content:', response.json())

    endpoint = URL + f'/zoo/{id2}'

    response = requests.get(endpoint)
    assert response.status_code == 200
    assert response.json() == animal2

    print()
    print('GET', endpoint)
    print('Status_code:', response.status_code)
    print('Response content:', response.json())

    endpoint = URL + '/zoo/'

    response = requests.get(endpoint)
    assert response.status_code == 200
    assert response.json() == [animal1, animal2] or response.json() == [animal2, animal1]

    print()
    print('GET', endpoint)
    print('Status_code:', response.status_code)
    print('Response content:', response.json())

    endpoint = URL + f'/zoo/{id2}'

    response = requests.delete(endpoint)
    assert response.status_code == 200
    assert response.content == f'Animal ({id2}) now not in the Zoo!'.encode()

    print()
    print('DELETE', endpoint)
    print('Status_code:', response.status_code)
    print('Response content:', response.content)

    endpoint = URL + f'/zoo/'

    response = requests.get(endpoint)
    assert response.status_code == 200
    assert response.json() == [animal1]

    print()
    print('GET', endpoint)
    print('Status_code:', response.status_code)
    print('Response content:', response.json())

    endpoint = URL + f'/zoo/{id1}'

    response = requests.put(endpoint, json=animal2)
    assert response.status_code == 200
    assert response.content == f'Animal ({id1}) replaced!'.encode()

    print()
    print('PUT', endpoint)
    print('Status_code:', response.status_code)
    print('Response content:', response.content)

    response = requests.get(endpoint)
    assert response.status_code == 200
    assert response.json() == animal2

    print()
    print('GET', endpoint)
    print('Status_code:', response.status_code)
    print('Response content:', response.json())

    update = {'description': 'Play with him at least once a day!'}

    response = requests.patch(endpoint, json=update)
    assert response.status_code == 200
    assert response.content == f'Animal ({id1}) patched!'.encode()

    print()
    print('PATCH', endpoint)
    print('Status_code:', response.status_code)
    print('Response content:', response.content)

    animal2.update(update)

    response = requests.get(endpoint)
    assert response.status_code == 200
    assert response.json() == animal2

    print()
    print('GET', endpoint)
    print('Status_code:', response.status_code)
    print('Response content:', response.json())

    endpoint = URL + f'/zoo/'

    response = requests.options(endpoint)
    assert response.status_code == 200
    assert response.content == b''
    assert set(map(str.strip, response.headers['Allow'].split(','))) == {'GET', 'HEAD', 'OPTIONS', 'POST'}

    print()
    print('OPTIONS', endpoint)
    print('Status_code:', response.status_code)
    print('Response content:', response.content)
    print('Response headers["Allow"]:', response.headers['Allow'])

    endpoint = URL + f'/zoo/{id1}'

    response = requests.options(endpoint)
    assert response.status_code == 200
    assert response.content == b''
    assert set(map(str.strip, response.headers['Allow'].split(','))) == {'PATCH', 'PUT', 'HEAD', 'DELETE', 'OPTIONS', 'GET'}

    print()
    print('OPTIONS', endpoint)
    print('Status_code:', response.status_code)
    print('Response content:', response.content)
    print('Response headers["Allow"]:', response.headers['Allow'])

    endpoint = URL + f'/zoo/'

    response = requests.head(endpoint)
    assert response.status_code == 200
    assert response.content == b''

    print()
    print('HEAD', endpoint)
    print('Status_code:', response.status_code)
    print('Response content:', response.content)
    print('Response headers:', response.headers)

    endpoint = URL + f'/zoo/{id1}'

    response = requests.head(endpoint)
    assert response.status_code == 200
    assert response.content == b''

    print()
    print('HEAD', endpoint)
    print('Status_code:', response.status_code)
    print('Response content:', response.content)
    print('Response headers:', response.headers)
