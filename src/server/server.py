import os
import hashlib
from flask import Flask, request
from marshmallow import Schema, fields, RAISE

app = Flask(__name__)

class Animal(Schema):
    name = fields.Str(required=True)
    food = fields.List(fields.Str, required=True)
    description = fields.Str(required=False)

    def __init__(self):
        super().__init__(unknown=RAISE)

class GetID():
    seq_n = 0
    salt = 'skdlaksdjdjkfslf'

    def next_id(self):
        self.seq_n += 1
        return hashlib.sha1((self.salt + str(self.seq_n)).encode()).hexdigest()

animals = {}
get_id = GetID()
animal_schema = Animal()

@app.route('/zoo/', methods=['GET', 'POST'])
def root_func():
    if request.method == 'GET':
        resp = animal_schema.dumps(animals.values(), many=True)
        return resp
    elif request.method == 'POST':
        data = animal_schema.load(request.json)
        animal_id = get_id.next_id()
        animals[animal_id] = data
        return {'animal_id': animal_id}
    else:
        return 'Unknown request!'

@app.route('/zoo/<string:animal_id>', methods=['GET', 'PUT', 'PATCH', 'DELETE'])
def animal_func(animal_id):
    if animal_id not in animals:
        return f"Wrong request! Animal ({animal_id}) doesn't exist!", 400
    elif request.method == 'GET':
        resp = animal_schema.dumps(animals[animal_id])
        return resp
    elif request.method == 'PUT':
        data = animal_schema.load(request.json)
        animals[animal_id] = data
        return f'Animal ({animal_id}) replaced!'
    elif request.method == 'PATCH':
        data = animal_schema.load(request.json, partial=True)
        animals[animal_id].update(data)
        return f'Animal ({animal_id}) patched!'
    elif request.method == 'DELETE':
        animals.pop(animal_id)
        return f'Animal ({animal_id}) now not in the Zoo!'
    else:
        return 'Unknown request!'

app.run(host='0.0.0.0', port=int(os.environ.get('HSE_HTTP_FLASK_PORT', 80)))
